import discord
from discord.ext import commands
from discord import app_commands
import datetime
import aiosqlite
import asyncio
import wavelink

client = commands.Bot('bot.', intents = discord.Intents.all())

@client.event
async def on_ready():
    print('client is ready to start and now beginning to sync commands.')
    await client.change_presence(activity=discord.activity.Streaming(name='24/7 commands.', url='https://google.com'))

    try:
        sync = await client.tree.sync()
        print(f'Synced {len(sync)} commands.')
    except Exception as e:
        print(f'Oops! An error occured: {e}')
    
    print('If no error occured, the client will now connect to the database.')
    
    
    db = await aiosqlite.connect('warns.db')
    await asyncio.sleep(3)
    async with db.cursor() as cursor:
        await cursor.execute('''CREATE TABLE IF NOT EXISTS warns(user INTEGER, reason TEXT, time INTEGER, guild INTEGER)''')
    await db.commit()

async def addwarn(ctx, reason, user):
    db = await aiosqlite.connect('warns.db')
    async with db.cursor() as cursor:
        await cursor.execute('''INSERT INTO warns(user, reason, time, guild) VALUES(?, ?, ?, ?)''', (user.id, reason, int(datetime.datetime.now().timestamp()), ctx.guild.id))
    await db.commit()

async def removewarn(ctx, user, reason):
    db = await aiosqlite.connect('warns.db')
    async with db.cursor() as cursor:
        await cursor.execute('''DELETE FROM warns WHERE user = ? AND reason = ? AND guild = ?''', (user.id, reason, ctx.guild.id, ))
    await db.commit()

async def getwarns(ctx, user):
    db = await aiosqlite.connect('warns.db')
    async with db.cursor() as cursor:
        await cursor.execute('''SELECT * FROM warns WHERE user = ? AND guild = ?''', (user.id, ctx.guild.id))
        data = await cursor.fetchall()

@client.tree.command(name = 'warn', description = 'Warns a user.')
async def warn(interaction: discord.Interaction, user: discord.Member, *, reason: str = 'A reason was not provided.'):
    # Check if the suer has the manage_permissions permission
    if interaction.user.guild_permissions.manage_messages == True:
        await addwarn(interaction, reason, user)
        
        embed = discord.Embed(
            title = 'Moderation System',
            description = f'{user.mention} has been warned for {reason}.',
            color = discord.Color.red()
        )
        await interaction.response.send_message(embed = embed)
    else:
        embed = discord.Embed(
            title = 'Moderation System',
            description = 'You do not have the manage_messages permission.',
            color = discord.Color.red()
        )
        await interaction.response.send_message(embed = embed)

@client.tree.command(name = 'removewarn', description = 'Removes a warn from a user.')
async def removewarn(interaction: discord.Interaction, user: discord.Member, *, reason: str = 'A reason was not provided.'):
    # Check if the suer has the manage_permissions permission
    if interaction.user.guild_permissions.manage_messages == True:
        await removewarn(interaction, user, reason)
        
        embed = discord.Embed(
            title = 'Moderation System',
            description = f'{user.mention} has been removed from the warn list for {reason}.',
            color = discord.Color.red()
        )
        await interaction.response.send_message(embed = embed)
    else:
        embed = discord.Embed(
            title = 'Moderation System',
            description = 'You do not have the manage_messages permission.',
            color = discord.Color.red()
        )
        await interaction.response.send_message(embed = embed)

@client.tree.command(name = 'warns', description = 'Shows the warns of a user.')
async def warns(interaction: discord.Interaction, user: discord.Member):
    # Check if the suer has the manage_permissions permission
    if interaction.user.guild_permissions.manage_messages == True:
        embed = discord.Embed(
            title = 'Moderation System',
            description = f'Here is a lsit of {user.mention}\'s warns.',
            colour = discord.Colour.green()
        )

        warn_num = 0

        db = await aiosqlite.connect('warns.db')
        async with db.cursor() as cursor:
            await cursor.execute('''SELECT * FROM warns WHERE user = ? AND guild = ?''', (user.id, interaction.guild.id))
            data = await cursor.fetchall()
        
        if data:

            for table in data:
                warn_num += 1
                embed.add_field(name = f'{warn_num}', value = f'Reason: {table[1]} | Date Issued: <t:{int(table[2])}:F>.')

                await interaction.response.send_message(embed = embed)
        else:
            embed.description = f'{user.mention} has no warns.'

@client.tree.command()
async def purge(interaction: discord.Interaction, *, amount: int = 10):
    # Check if the suer has the manage_permissions permission
    if interaction.user.guild_permissions.manage_messages == True:
        await interaction.channel.purge(limit = amount)
        embed = discord.Embed(
            title = 'Moderation System',
            description = f'{amount} messages have been deleted.',
            colour = discord.Colour.green()
        )
        await interaction.response.send_message(embed = embed)
    else:
        embed = discord.Embed(
            title = 'Moderation System',
            description = 'You do not have the manage_messages permission.',
            colour = discord.Colour.red()
        )
        await interaction.response.send_message(embed = embed)

client.run('OTg2NzMwMzk3OTY4Nzg1NTE4.GrmMIs.Ax7OOpBFvIlyyJBfSH1vhSPNSO2EZY-ZEpoSzA')